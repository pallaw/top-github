package com.pallaw.top_github.viewmodel.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pallaw.top_github.data.Remote.RemoteDataManager

/**
 * Created by Pallaw Pathak on 2020-02-09. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class ViewModelFactory(val remoteDataManager: RemoteDataManager): ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(RemoteDataManager::class.java).newInstance(remoteDataManager)
    }
}
package com.pallaw.top_github.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.data.Remote.BaseResponseObserver
import com.pallaw.top_github.data.Remote.RemoteDataManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by Pallaw Pathak on 2020-02-09. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class DevelopersListViewModel(private val remoteDataManager: RemoteDataManager) : ViewModel() {
    var developerListLive: MutableLiveData<List<Developer>> =
        MutableLiveData<List<Developer>>()

    init {
        downloadDeveloperList()
    }

    fun downloadDeveloperList(): Unit {
        remoteDataManager.apiService.fetchDevelopers("java", "weekly")
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : BaseResponseObserver<List<Developer>>() {
                override fun onResponse(response: List<Developer>) {
                    developerListLive.value = response
                }

                override fun onServerError(e: Throwable) {
                    e.printStackTrace()
                }
            })
    }
}
package com.pallaw.top_github.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.pallaw.top_github.data.Model.Developer

/**
 * Created by Pallaw Pathak on 2020-02-11. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class DeveloperDetailViewModel(private val developer: Developer) : ViewModel() {

    val liveDeveloperImageUrl = MutableLiveData<String>()
    val liveDeveloperName = MutableLiveData<String>()
    val liveDeveloperUserName = MutableLiveData<String>()
    val liveDeveloperProfileUrl = MutableLiveData<String>()
    val liveRepoName = MutableLiveData<String>()
    val liveRepoUrl = MutableLiveData<String>()
    val liveRepoDescription = MutableLiveData<String>()

    init {
        liveDeveloperImageUrl.value = developer.avatar
        liveDeveloperName.value = developer.name
        liveDeveloperUserName.value = developer.username
        liveDeveloperProfileUrl.value = developer.url
        liveRepoName.value = developer.repo.name
        liveRepoUrl.value = developer.repo.url
        liveRepoDescription.value = developer.repo.description
    }

}
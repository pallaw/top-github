package com.pallaw.top_github.ui.developers

import android.widget.TextView
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.ui.custom.CircleImageView

interface RecyclerLayoutClickListener {

    fun redirectToDetailScreen(
        item: Developer,
        sharedImageView: CircleImageView,
        sharedRepoNameView: TextView,
        sharedUserNameView: TextView
    )

}

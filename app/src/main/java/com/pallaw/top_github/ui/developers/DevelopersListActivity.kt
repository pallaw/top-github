package com.pallaw.top_github.ui.developers

import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.pallaw.top_github.R
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.data.Remote.RemoteDataManager
import com.pallaw.top_github.databinding.ActivityDevelopersListBinding
import com.pallaw.top_github.ui.custom.CircleImageView
import com.pallaw.top_github.util.NavigatorUtils
import com.pallaw.top_github.viewmodel.DevelopersListViewModel
import com.pallaw.top_github.viewmodel.factory.ViewModelFactory


class DevelopersListActivity : AppCompatActivity(), RecyclerLayoutClickListener {

    private lateinit var viewModel: DevelopersListViewModel
    private lateinit var binding: ActivityDevelopersListBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Binding views and viewModel
        setupBinding()

        //setup developer list recyclerView
        setupDeveloperList()

    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.let { setTitle(R.string.app_name) }
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_developers_list)
        viewModel = ViewModelProviders.of(
            this,
            ViewModelFactory(RemoteDataManager(this))
        ).get(
            DevelopersListViewModel::class.java
        )
        binding.developersListViewModel = viewModel
        setSupportActionBar(binding.toolbar)
    }

    private fun setupDeveloperList() {

        viewModel.developerListLive.observe(this, Observer<List<Developer>> {
            binding.contentView.recyclerView.layoutManager = LinearLayoutManager(this)
            val adapter = DeveloperListAdapter(it, this)
            binding.contentView.recyclerView.adapter = adapter
        })

        viewModel.downloadDeveloperList()
    }

    override fun redirectToDetailScreen(
        item: Developer,
        sharedImageView: CircleImageView,
        sharedRepoNameView: TextView,
        sharedUserNameView: TextView
    ) {
        val imageTransition = androidx.core.util.Pair<View, String>(sharedImageView, ViewCompat.getTransitionName(sharedImageView)!!)
        val repoNameTransition = androidx.core.util.Pair<View, String>(sharedImageView, ViewCompat.getTransitionName(sharedRepoNameView)!!)
        val userNameTransition = androidx.core.util.Pair<View, String>(sharedImageView, ViewCompat.getTransitionName(sharedUserNameView)!!)

        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, imageTransition, repoNameTransition, userNameTransition)
        NavigatorUtils.redirectToDetailScreen(this, item, options, sharedImageView, sharedRepoNameView, sharedUserNameView)
    }
}

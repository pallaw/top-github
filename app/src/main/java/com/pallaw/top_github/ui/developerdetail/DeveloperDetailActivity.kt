package com.pallaw.top_github.ui.developerdetail

import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.pallaw.top_github.R
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.databinding.ActivityDevelopersDetailBinding
import com.pallaw.top_github.util.NavigatorUtils
import com.pallaw.top_github.viewmodel.DeveloperDetailViewModel
import com.pallaw.top_github.viewmodel.factory.DeveloperDetailViewModelFactory


class DeveloperDetailActivity : AppCompatActivity() {

    private lateinit var binding: ActivityDevelopersDetailBinding
    private lateinit var viewModel: DeveloperDetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setupBinding()

        intent.extras?.let {
            val item: Developer = it.getSerializable(NavigatorUtils.ARG_ITEM) as Developer

            //handle transitions
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val transitionImage: String = it.getString(NavigatorUtils.ARG_TRANSITION_IMAGE)!!
                val transitionRepoName: String = it.getString(NavigatorUtils.ARG_TRANSITION_REPO_NAME)!!
                val transitionUserName: String = it.getString(NavigatorUtils.ARG_TRANSITION_USER_NAME)!!

                binding.contentView.detailImgProfile.transitionName = transitionImage
                binding.contentView.detailTxtRepoName.transitionName = transitionRepoName
                binding.contentView.detailTxtDevUserName.transitionName = transitionUserName

            }

            //create view model with item
            createViewModel(item)
        }

    }

    private fun createViewModel(item: Developer) {
        viewModel = ViewModelProviders.of(
            this,
            DeveloperDetailViewModelFactory(item)
        ).get(
            DeveloperDetailViewModel::class.java
        )
        binding.developersDetailViewModel = viewModel

        showData()
    }

    private fun showData() {
        viewModel.liveDeveloperImageUrl.observe(this,
            Observer<String> { url ->
                Glide.with(binding.root.context)
                    .load(url)
                    .placeholder(R.drawable.ic_dp_placeholder)
                    .into(binding.contentView.detailImgProfile)
            })
    }

    override fun onResume() {
        super.onResume()
        supportActionBar?.let { setTitle(R.string.title_detail) }
    }

    private fun setupBinding() {
        binding = DataBindingUtil.setContentView(this, R.layout.activity_developers_detail)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.run { setDisplayHomeAsUpEnabled(true) }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            android.R.id.home -> { onBackPressed() }
        }
        return super.onOptionsItemSelected(item)
    }

}

package com.pallaw.top_github.ui.developers

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.ViewCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.pallaw.top_github.R
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.databinding.DeveloperListItemBinding

/**
 * Created by Pallaw Pathak on 2020-02-10. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class DeveloperListAdapter(val listItems: List<Developer>, val listItemClickListener: RecyclerLayoutClickListener) :
    RecyclerView.Adapter<DeveloperListAdapter.DeveloperViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DeveloperViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = DeveloperListItemBinding.inflate(layoutInflater, parent, false)
        val developerViewHolder = DeveloperViewHolder(binding)

        binding.cardView.setOnClickListener{view -> developerViewHolder.onItemClicked()}

        return developerViewHolder
    }

    override fun getItemCount(): Int {
        return listItems.count()
    }


    override fun onBindViewHolder(holder: DeveloperViewHolder, position: Int) {
        holder.bindTo(listItems[position])
    }

    inner class DeveloperViewHolder(val binding: DeveloperListItemBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bindTo(item: Developer) {
            binding.itemDeveloperTxtRepoName.text = item.repo.name
            binding.itemDeveloperTxtUserName.text = "Username: ${item.username}"

            ViewCompat.setTransitionName(binding.itemDeveloperTxtRepoName, item.repo.name)
            ViewCompat.setTransitionName(binding.itemDeveloperTxtUserName, item.username)
            ViewCompat.setTransitionName(binding.itemDeveloperImgDp, item.avatar)

            Glide.with(binding.root.context)
                .load(item.avatar)
                .placeholder(R.drawable.ic_dp_placeholder)
                .into(binding.itemDeveloperImgDp)
        }

        fun onItemClicked() {
            listItemClickListener.redirectToDetailScreen(getItem(layoutPosition), binding.itemDeveloperImgDp, binding.itemDeveloperTxtRepoName, binding.itemDeveloperTxtUserName)
        }

    }

    private fun getItem(layoutPosition: Int): Developer {
        return listItems[layoutPosition]
    }
}


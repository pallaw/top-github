package com.pallaw.top_github.util

import android.app.Activity
import android.content.Intent
import android.net.Uri
import android.widget.TextView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.view.ViewCompat
import com.pallaw.top_github.data.Model.Developer
import com.pallaw.top_github.ui.custom.CircleImageView
import com.pallaw.top_github.ui.developerdetail.DeveloperDetailActivity


object NavigatorUtils {

    public val ARG_TRANSITION_IMAGE: String = "arg_transition_image"
    public val ARG_TRANSITION_REPO_NAME: String = "arg_transition_repo_name"
    public val ARG_TRANSITION_USER_NAME: String = "arg_transition_user_name"
    public const val ARG_ITEM: String = "arg_item"

    fun redirectToDetailScreen(
        activity: Activity,
        developer: Developer,
        options: ActivityOptionsCompat,
        sharedImageView: CircleImageView,
        sharedRepoNameView: TextView,
        sharedUserNameView: TextView
    ) {

        val intent = Intent(activity, DeveloperDetailActivity::class.java)
        intent.putExtra(ARG_ITEM, developer)

        //add transitions extras
        intent.putExtra(ARG_TRANSITION_IMAGE, ViewCompat.getTransitionName(sharedImageView))
        intent.putExtra(ARG_TRANSITION_REPO_NAME, ViewCompat.getTransitionName(sharedRepoNameView))
        intent.putExtra(ARG_TRANSITION_USER_NAME, ViewCompat.getTransitionName(sharedUserNameView))

        activity.startActivity(intent, options.toBundle())
    }

    fun openBrowser(activity: Activity, url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        activity.startActivity(i)
    }
}

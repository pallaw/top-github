package com.pallaw.top_github.data.Remote

import com.pallaw.top_github.data.Model.Developer
import io.reactivex.Observable
import okhttp3.ResponseBody
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

/**
 * Created by Pallaw Pathak on 2020-02-09. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
interface ApiServices {
    @GET("developers")
    fun fetchDevelopers(@Query("language") language: String, @Query("since") since: String): Observable<List<Developer>>

    @GET
    fun downloadImage(@Url url: String): Observable<ResponseBody>
}
package com.pallaw.top_github.data.Remote

import io.reactivex.Observer
import io.reactivex.disposables.Disposable

/**
 * Created by Pallaw Pathak on 2020-02-10. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
abstract class BaseResponseObserver<T> : Observer<T> {

    override fun onComplete() {
    }

    override fun onSubscribe(d: Disposable) {
    }

    override fun onNext(t: T) {
        onResponse(t)
    }

    override fun onError(e: Throwable) {
        onServerError(e)
    }

    abstract fun onResponse(response: T)

    abstract fun onServerError(e: Throwable)
}
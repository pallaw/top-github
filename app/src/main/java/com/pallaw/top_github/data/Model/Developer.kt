package com.pallaw.top_github.data.Model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

/**
 * Created by Pallaw Pathak on 2020-02-09. - https://www.linkedin.com/in/pallaw-pathak-a6a324a1/
 */
class Developer(
    @SerializedName("avatar")
    var avatar: String = "",
    @SerializedName("name")
    var name: String = "",
    @SerializedName("repo")
    var repo: Repo = Repo(),
    @SerializedName("type")
    var type: String = "",
    @SerializedName("url")
    var url: String = "",
    @SerializedName("username")
    var username: String = ""
): Serializable